{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign
import Foreign.C.Types
import Foreign.C.String

foreign import ccall "v8wrapper.h runv8"
     c_runv8 :: CString -> CString

run :: String -> IO String
run x = do
   str <- newCString x 
   result <- peekCString (c_runv8 str)
   return result

main = run "25 * 25"
