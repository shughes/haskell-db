var http = require('http');
var util = require('util');
var events = require('events');


DB = function(name, callback) { 
   this.name = name;
   this.host = 'localhost';
   this.port = 8888;
   this.client = http.createClient(this.port, this.host);
   var db = this;

   var client = this.client;
   var path = '/tables/'+this.name;
   var req = client.request('GET', path, {'host': this.host});
   req.end();
   req.on('response', function(response) {
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
         var tables = eval('('+chunk+')');
         for(var i=0; i < tables.length; i++) {
            var t = new Table(db, tables[i]);
         }
         db.emit('ready');
         if(callback) {
            callback(chunk);
         }
      });
   });
};

util.inherits(DB, events.EventEmitter);

DB.prototype.getName = function() { 
   return this.name; 
}

DB.prototype.getClient = function() {
   return this.client;
}


DB.prototype.getHost = function() { 
   return this.host;
}

DB.prototype.add = function(table, callback) {
   var client = this.client;
   var path = '/table/'+this.name+'/'+table;
   var db = this;
   var options = {
      host: db.host,
      port: db.port,
      path: path,
      method: 'POST',
   };
   var req = http.request(options, function(res) {
      res.setEncoding('utf8');
      res.on('data', function(chunk) {
         db[table] = new Table(db, table);
         if(callback) {
            callback(chunk);
         }
      });
   });
   req.write('\n');
   req.end();
}


Table = function(db, name) {
   this.db = db;
   this.name = name;
   this.db[name] = this;
}

util.inherits(Table, events.EventEmitter);


Table.prototype.getDB = function() {
   return this.db;
}

Table.prototype.getName = function() {
   return this.name;
}

Table.prototype.putIndexes = function(data, reindex, callback) {
   var path = '/indexes/'+this.db.getName()+'/'+this.name;
   var db = this.db;
   var strData = JSON.stringify(data);
   var table = this;
   var options = {
      host: db.host,
      port: db.port,
      path: path,
      method: 'PUT',
      headers: {
         'Content-Type': 'application/json',
         'Content-Length': strData.length
      }
   };
   var req = http.request(options, function(response) {
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
         if(reindex) {
            options.headers = {};
            options.path = '/reindex/'+db.getName()+'/'+table.name;
            var req2 = http.request(options, function(response2) {
               response2.on('data', function(chunk2) {
                  if(callback) {
                     callback(chunk2);
                  }
               });
            });
            req2.end();
         } else {
            if(callback) {
               callback(chunk);
            }
         }
      });
   });
   req.write(strData);
   req.end();
   return [];
}

Table.prototype.find = function(options, callback) {
   var client = this.db.getClient();
   var path = '/allRecords/'+this.db.getName()+'/'+this.name;
   var req = client.request('GET', path, {'host': this.db.getHost()});
   var table = this;
   req.end();
   req.on('response', function(response) {
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
         var arr = eval('('+chunk+')');
         table.emit('ready', arr);
         if(callback) {
            callback(arr);
         }
      });
   });

   return { 
      sort: function() { console.log("sort"); },
      limit: function() { console.log("limit"); }
   };
}

Table.prototype.add = function(data, callback) {
   var client = this.db.getClient();
   var path = '/data/'+this.db.getName()+'/'+this.name;
   var strData = JSON.stringify(data);
   var table = this;
   var db = this.db;
   var method = 'POST';
   var options = {
      host: db.host,
      port: db.port,
      path: path,
      method: method,
      headers: {
         'Content-Type': 'application/json',
         'Content-Length': strData.length
      }
   };
   var req = http.request(options, function(response) {
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
         if(callback) {
            callback(chunk);
         }
      });
   });
   req.write(strData);
   req.end();
   return [];
}

Table.prototype.save = function(data, callback) {
   var client = this.db.getClient();
   var path = '/data/'+this.db.getName()+'/'+this.name;
   var strData = JSON.stringify(data);
   var table = this;
   var db = this.db;
   var method = 'PUT';
   var options = {
      host: db.host,
      port: db.port,
      path: path,
      method: method,
      headers: {
         'Content-Type': 'application/json',
         'Content-Length': strData.length
      }
   };
   var req = http.request(options, function(response) {
      response.setEncoding('utf8');
      response.on('data', function(chunk) {
         if(callback) {
            callback(chunk);
         }
      });
   });
   req.write(strData);
   req.end();
   return [];
}

Table.prototype.mapReduce = function(map, reduce, options, callback) {
   var mapResult = [];
   var table = this;
   table.find({}, function(arr) {
      for(var i = 0; i < arr.length; i++) {
         var tmp = map(arr[i]);
         var key = tmp[0];
         var value = tmp[1];
         if(mapResult[key] == null) {
            mapResult[key] = [value];
         } else {
            mapResult[key].push(value);
         }
      }
      var reduceResult = [];
      for(var k in mapResult) {
         var tmp = {
            "key": k,
            "value": reduce(k, mapResult[k])
         };
         reduceResult.push(tmp);
      }

      var db = table.getDB();
      db.add(options.out, function(e) {
         var newTable = db[options.out];
         var key = "_key";
         newTable.putIndexes([key], false, function(e) {
            var rows = [];
            for(var i in reduceResult) {
               var row = reduceResult[i];
               var record = row.value;
               record[key] = row.key;
               rows.push(record);
            }
            newTable.add(rows, function(e) {
               if(callback) {
                  callback(newTable);
               }
            });
         });
      });
   });
}


var map = function(user) { 
   return [user.author, {votes: user.votes}];
};

var reduce = function(key, values) { 
   var sum = 0;
   for(var i in values) {
      sum += values[i].votes;
   }
   return {votes: sum};
};


var db = new DB("db");

db.on('ready', function() {
   //db.users.mapReduce(map, reduce, {out: "uservotes"});
   db.add("users", function(e) {
      var data = [
         {"author": "Sam Hughes", "votes": 5},
         {"author": "John Freker", "votes": 8},
         {"author": "Sam Hughes", "votes": 23},
         {"author": "David Hughes", "votes": 9},
         {"author": "John Freker", "votes": 15}
      ];
      db.users.add(data, function(e) {
         //db.users.mapReduce(map, reduce, {out: "uservotes"});
      });
   });
});

