module Db.TreeIndex where

import Db.Table
import Db.Common
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put

data TreeIndex = INodeIndex BIndex
               | INodeDataIndex BIndex
               | TableIndex BIndex BIndex
               | TableRowIndex BIndex String TableColumn
                 deriving (Show, Eq, Ord)

instance Serialize TreeIndex where
  put (INodeIndex i) = do
    putWord8 0
    put i
  
  put (INodeDataIndex i) = do
    putWord8 1
    put i
  
  put (TableIndex i j) = do
    putWord8 2
    put i
    put j
  
  put (TableRowIndex i s c) = do
    putWord8 3
    put i 
    put s
    put c
  
  get = do    
    ri <- getWord8
    case ri of
      0 -> do i <- get :: Get BIndex
              return $ INodeIndex i
      1 -> do i <- get :: Get BIndex
              return $ INodeDataIndex i
      2 -> do i <- get :: Get BIndex
              j <- get :: Get BIndex
              return $ TableIndex i j
      3 -> do i <- get :: Get BIndex
              s <- get :: Get String              
              c <- get :: Get TableColumn
              return $ TableRowIndex i s c
      
