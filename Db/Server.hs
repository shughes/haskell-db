module Db.Server where

import Db.Common
import Db.Fs
import Db.ManagedContext
import Db.BTreeData
import Db.Table
import Db.File
import Db.FileContext
import Data.Typeable
import Data.Ratio
import Db.JSON
import qualified Data.Map as M
import Control.Concurrent
import Control.Monad
import Network
import Network.Shed.Httpd
import System.IO
import qualified Data.List as L
import qualified Data.ByteString as B

port = 8888
 
handleRequest context tid (Request "GET" uri headers body) = do
   case breakn '/' (show uri) of
        ("test" : _) -> do
           let responseHeaders = [("Set-Cookie", "name=value")]
           return $ Response 200 responseHeaders (show headers)

        ("disconnect" : _) -> do 
            treeCommit context
            hClose $ fileHandle $ managedFile context
            killThread tid
            return $ Response 200 defaultHeaders "shutdown"
        
        ("tables" : db : _) -> do
           tables <- getTableNames context ("/"++db)
           return $ Response 200 defaultHeaders $ show tables

        ("lastRow" : db : table : _) -> do
           let path = ("/"++db++"/"++table)
           size <- getLastRowid context path
           return $ Response 200 defaultHeaders ("{\"rowid\": "++(show size)++"}")
            

        ("record" : db : table : srowid : _) -> do
            let rowid = ((read srowid)::BIndex)
            mrow <- getTableRow context ("/"++db++"/"++table) rowid
            result <- return $ maybe "[]" 
               (\ row -> encode $ tableRowToJson rowid row) 
               mrow
            return $ Response 200 defaultHeaders result
        
        ("recordsByIndex" : db : table : field : dtype : value1 : _) -> do
           let value = case dtype of
                            "number" -> TableNumber ((read value1)::BIndex)
                            "text" -> TableText value1
               path = "/"++db++"/"++table
           rows <- getTableRowsByIndex context path field value
           return $ Response 200 defaultHeaders (show rows)
        
        ("allRecords" : db : table : _) -> do
           rows <- getAllRecords context ("/"++db++"/"++table)
           if L.length rows > 0 
              then do json1 <- return $ map (\(rowid, row)-> tableRowToJson rowid row) rows
                      let json2 = initArray json1
                      return $ Response 200 defaultHeaders (encode json2)
              else return $ Response 400 defaultHeaders returnFailed


        ("records" : db : table : smin : smax : _) -> do
            let minrow = ((read smin)::BIndex)
                maxrow = ((read smax)::BIndex)
            rows <- getTableRows context ("/"++db++"/"++table) minrow maxrow
            json1 <- return $ map (\(rowid, row) -> tableRowToJson rowid row) rows
            let json2 = initArray json1
            return $ Response 200 defaultHeaders (encode json2)

        otherwise -> return $ Response 400 defaultHeaders returnFailed


handleRequest context tid (Request "POST" uri headers body) = do
   case breakn '/' (show uri) of
        ("db" : db : _) -> do
            addDb context ("/"++db) 
            return $ Response 200 defaultHeaders returnSuccess

        ("table" : db : table : _) -> do
           let path = "/"++db++"/"++table
           mtable <- getTable context path
           case mtable of
                Nothing -> do putTable context path 0 EmptySchema
                              return $ Response 200 defaultHeaders returnSuccess
                otherwise -> return $ Response 400 defaultHeaders returnFailed

        -- 
        -- example: [{"field1": "value 1"}, {"field1": "value 1"}]
        --
        ("data" : db : table : _) -> do
            let rows = jsonToTableRows body
                path = "/"++db++"/"++table
                doErr = foldl (\ err (TableRow val) -> 
                  let mexists = M.lookup "_id" val
                      curRowErr = maybe err (\ _ -> True) mexists
                  in if err == True
                        then True
                        else curRowErr) False rows
            if doErr
               then return $ Response 400 defaultHeaders returnFailed
               else do addTableRows context path rows 
                       return $ Response 200 defaultHeaders returnSuccess

        -- 
        -- Big searches can be POST with query data in the POST body.
        --
        -- example: {"field": "<field name>", "value": "<field value>"}
        --
        ("recordsByIndex" : db : table : _) -> do
           let (Right jqueryMap) = parseJson body
               queryMap = jobject jqueryMap
               path = "/" ++ db ++ "/" ++ table
               mfield = M.lookup "field" queryMap
               mvalue = M.lookup "value" queryMap
           rows <- case (mfield, mvalue) of
                        (Just jfield, Just jval) -> do
                           let val = convertVal jval
                               field = jstring jfield
                           getTableRowsByIndex context path field val
                        otherwise -> return []
           let json1 = map (\(rowid, row)-> tableRowToJson rowid row) rows
               json2 = initArray json1
           return $ Response 200 defaultHeaders (encode json2)

        otherwise -> return $ Response 400 defaultHeaders returnFailed

handleRequest context tid (Request "PUT" uri headers body) = do
   case breakn '/' (show uri) of
        ("commit" : _) -> do
           treeCommit context
           return $ Response 200 defaultHeaders returnSuccess

        ("data" : db : table : _) -> do
            let rows = jsonToTableRows body
            let path = "/"++db++"/"++table
            result2 <- foldl 
               (\ ret (TableRow rowmap) -> do
                  arr <- ret
                  let mrowid = M.lookup "_id" rowmap
                  maybe (return 0)
                     (\ (TableRowid rowid) -> do
                        let newMap = M.delete "_id" rowmap
                        let newRow = TableRow newMap
                        putTableRow context path rowid newRow
                        return rowid)
                     mrowid)
               (return 0) rows
            return $ Response 200 defaultHeaders returnSuccess

        --
        -- example: ["index1", "index2"]
        --
        ("indexes" : db : table : _) -> do
           let path =  "/"++db++"/"++table
           let indexes = jsonToIndexes body
           putIndexes context path indexes
           -- WHY??
           treeFlush context
           return $ Response 200 defaultHeaders returnSuccess

        ("reindex" : db : table : _) -> do
           let path = "/"++db++"/"++table
           reindex context path
           return $ Response 200 defaultHeaders returnSuccess

        otherwise -> return $ Response 400 defaultHeaders returnFailed

handleRequest context tid (Request "DELETE" uri headers body) = do
   case breakn '/' (show uri) of
        ("db" : db : _) -> do
            deleteDb context ("/"++db)
            return $ Response 200 defaultHeaders returnSuccess

        ("record" : db : table : srowid : _) -> do
            let rowid = ((read srowid)::BIndex)
            deleteTableRow context ("/"++db++"/"++table) rowid
            return $ Response 200 defaultHeaders returnSuccess 

        ("table" : db : table : _) -> do
           let path = "/" ++ db ++ "/" ++ table
           deleteTable context path
           return $ Response 200 defaultHeaders returnSuccess

        otherwise -> return $ Response 400 defaultHeaders returnFailed

dbServer path = forkIO $ do 
   tid <- myThreadId 
   context <- initManagedContext path
   initServer port (handleRequest context tid)
   return () 

-- fmap :: (a -> b) -> M(a) -> M(b)

tableRowToJson rowid row = 
   let jrowid = initInt $ fromInteger rowid
       TableRow jmap = row
       jrow = initObject $ M.fromList $ map (\ key -> let Just rowVal = M.lookup key jmap
                                                          jval = jrowVal rowVal
                                                      in (key, jval)) 
                                        (M.keys jmap)
       rowmap = M.insert "_id" jrowid (jobject jrow)
   in initObject rowmap
   where
      jrowVal (TableText val) = initString val
      jrowVal (TableNumber val) = initInt val
      jrowVal (TableRowid val) = initInt val
      jrowVal (TableRowids arr) = initArray $ map initInt arr

jsonToIndexes body = 
   let eitherJson = parseJson body
   in either (\ _ -> [])
      (\  jarr -> map jstring (jarray jarr))
      eitherJson

jsonToTableRows body = 
   let eitherJson = parseJson body
   in either (\ err -> [])
         (\ jarr -> 
         map (\ jmap ->
         let cols = M.toList (jobject jmap)
             tableCols = map (\(key, val) -> (key, convertVal val)) cols
             result = TableRow $ M.fromList tableCols
         in result) (jarray jarr)) eitherJson

convertVal jval 
   | isString jval = TableText $ jstring jval
   | isInt jval = TableRowid $ jint jval

returnSuccess = "{\"result\": \"success\"}"

returnFailed = "(\"result\": \"failed\"}"

defaultHeaders = [("Content-Type", "application/json")]

