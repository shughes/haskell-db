module Db.FileContext 
    ( FileContext
    , fileHandle

    , getBPageF
    , initFileContext
    , newFileContext 
    , getFileContext )
    where

import Db.BPageF
import Db.BPage
import Db.BTreeData
import Db.Common
import Data.Serialize
import System.IO
import Data.Either
import qualified Data.ByteString as B

data FileContext = FileContext { fileHandle :: Handle }

instance BTreeData FileContext where
   putBPage (FileContext hdl) n page = do
      bpage <- return $ BPageTree (bRecords page) (bChildren page)
      putBPageF hdl n bpage
    
   getBPage (FileContext hdl) n = do
      bpagef <- getBPageF hdl n
      return $ maybe Nothing (\ (BPageTree r c) -> Just (newBPage r c)) bpagef
        
   putMaxBIndex (FileContext hdl) n = do
      bs <- return $ encode n
      hSeek hdl AbsoluteSeek (maxBPageMeta * 8)
      B.hPut hdl bs

   getMaxBIndex (FileContext hdl) = do
      hSeek hdl AbsoluteSeek (maxBPageMeta * 8)
      bs <- B.hGet hdl 8
      (Right bIndex) <- return ((decode bs)::Either String BIndex)
      return bIndex

   putMasterRoot (FileContext hdl) n = do
      hSeek hdl AbsoluteSeek (masterRootMeta * 8)
      bs <- return $ encode n
      B.hPut hdl bs
      hSeek hdl AbsoluteSeek (masterRootMetaCheck * 8)
      bsCheck <- return $ encode n
      B.hPut hdl bsCheck

   getMasterRoot (FileContext hdl) = do 
      hSeek hdl AbsoluteSeek (masterRootMeta * 8)
      bs <- B.hGet hdl 8
      (Right index) <- return ((decode bs)::Either String BIndex)
      return index

   nextBIndex context = do
      max <- getMaxBIndex context
      putMaxBIndex context (max+1)
      return (max+1)
  

newFileContext :: String -> IO FileContext
newFileContext file = do
   hdl <- openFile file ReadWriteMode 
   root <- return (2::BIndex)
   putBPage (FileContext hdl) root (newBPage [] [])
   putMaxBIndex (FileContext hdl) root
   putMasterRoot (FileContext hdl) root
   return $ FileContext hdl

initFileContext :: String -> IO FileContext
initFileContext file = do
   hdl <- openFile file ReadWriteMode
   return $ FileContext hdl

masterRootMeta :: BIndex
masterRootMeta = 1

masterRootMetaCheck :: BIndex
masterRootMetaCheck = 2

maxBPageMeta :: BIndex
maxBPageMeta = 0

pageSizeInt :: Int
pageSizeInt = fromIntegral pageSize

bpageIndex :: BIndex -> BIndex
bpageIndex n = metaData + n * pageSize

pageSize :: Integer
pageSize = 1024

metaData :: Integer
metaData = 100


getFileContext :: Handle -> FileContext
getFileContext hdl = FileContext hdl

addBIndex :: Handle -> IO BIndex
addBIndex hdl = do
   max <- getMaxBIndex (FileContext hdl)
   putMaxBIndex (FileContext hdl) (max+1)
   return $ max+1

putBPageF :: Handle -> BIndex -> BPageF -> IO ()
putBPageF hdl n bpage = do
   bs <- return $ encode bpage
   bsize <- return $ B.length bs
   case bsize >= pageSizeInt of
        True -> handleOverflowPut hdl bs n
        False -> do
           hSeek hdl AbsoluteSeek (bpageIndex n)
           B.hPut hdl bs

handleOverflowPut :: Handle -> B.ByteString -> BIndex -> IO ()
handleOverflowPut hdl bs n = do
   chunks <- return $ bsToChunks bs (pageSizeInt-25)
   recurChunks chunks n 
   where
      recurChunks (c:cs) idx = do
         nextIndex <- case cs of
                           [] -> return (0-1)
                           otherwise -> addBIndex hdl
         hSeek hdl AbsoluteSeek (bpageIndex idx)
         page <- return $ BPageOverflow c nextIndex
         bs <- return $ encode page
         B.hPut hdl bs
         case cs of
              [] -> return ()
              otherwise -> recurChunks cs nextIndex

getBPageF :: Handle -> BIndex -> IO (Maybe BPageF)
getBPageF hdl n = do
   hSeek hdl AbsoluteSeek (bpageIndex n)
   bs <- B.hGet hdl pageSizeInt
   case B.length bs of
        0 -> return Nothing
        otherwise -> finishInit bs
   where
      finishInit bs = do
         (Right treePage) <- return ((decode bs)::Either String BPageF)
         newBs <- case treePage of
                       (BPageOverflow bs' bi) -> return bs'
                       otherwise -> return B.empty
         result <- handleOverflowGet hdl treePage newBs []
         return $ Just result

bsToChunks :: B.ByteString -> Int -> [B.ByteString]
bsToChunks bs freeSpace = result
    where
      words = B.unpack bs
      idx = freeSpace - 1
      (a,b) = splitAt idx words
      bsa = B.pack a
      result = case (length b) == 0 of
                 True -> [bsa]
                 False -> [bsa] ++ (bsToChunks (B.pack b) freeSpace)


chunksToBs :: [B.ByteString] -> B.ByteString
chunksToBs arr = B.concat arr


handleOverflowGet :: Handle -> BPageF -> B.ByteString -> [BIndex] -> IO BPageF
handleOverflowGet hdl (BPageVersionList lst) newBs freeLst = return $ BPageVersionList lst
handleOverflowGet hdl (BPageTree r c) newBs freeLst = return $ BPageTree r c 
handleOverflowGet hdl (BPageOverflow bs bi) newBs freeLst = 
   case bi == (0-1) of
        True -> do (Right page1) <- return ((decode $ B.concat [newBs, bs])::Either String BPageF)
                   handleOverflowGet hdl page1 B.empty freeLst
        False -> do hSeek hdl AbsoluteSeek (bpageIndex bi)
                    bs' <- B.hGet hdl pageSizeInt
                    (Right page2) <- return ((decode bs')::Either String BPageF)
                    (BPageOverflow bs2 bi2) <- return page2
                    handleOverflowGet hdl (BPageOverflow bs2 bi2) 
                        (B.concat [newBs, bs2]) (bi:freeLst)
