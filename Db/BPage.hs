module Db.BPage 
   ( BPage
   , newBPage
   , bChildren
   , bRecords ) where


import Db.BRecord
import Db.Common

data BPage = BPage { bRecords :: [BRecord] 
                   , bChildren :: [BIndex] }
             deriving (Show)

newBPage recs childs = BPage recs childs

instance Eq BPage where
    p1 == p2 = and [((bRecords p1) == (bRecords p2)), 
      (bChildren p1) == (bChildren p2)] 
    p1 /= p2 = (p1 == p2) == False
