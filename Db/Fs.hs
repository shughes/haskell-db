module Db.Fs 
    ( putDb
    , addDb
    , deleteDb

    , putFile
    , getFile
    , addFile

    , getINodeData
    , getINode

    , putTable
    , getTable
    , getTableNames
    , deleteTable
    , getLastRowid

    , putTableRow
    , getTableRow
    , getTableRows
    , addTableRow
    , addTableRows
    , deleteTableRow

    , getTableRowsByIndex 
    , putIndexes
    , reindex

    , getAllRecords
    , initFs )
    where

import Db.Table
import Db.Common
import Db.BTreeData
import Db.BPage
import Db.BRecord
import Db.BDataType
import Db.File
import Db.TreeIndex
import Control.Exception
import Control.Monad
import Control.Monad.State
import System.IO
import Control.Concurrent.STM
import qualified Data.ByteString as B
import qualified Data.Map as M
import qualified Data.List as L

-- Example:
-- initManagedContext "test.db"
-- initFs c
-- addDb c "/db"
-- treeCommit c
--




putINode :: (BTreeData b) => b -> BIndex -> INode -> IO ()
putINode context nodePtr node = 
  putRecord context (INodeIndex nodePtr) (BINode node)

getINode :: (BTreeData b) => b -> BIndex -> IO (Maybe INode)
getINode context n = 
  do mrec <- getRecord context (BTreeIndex $ INodeIndex n)
     maybe (return Nothing) withRecord mrec
  where
    withRecord (BINode node) = return $ Just node

addINode :: (BTreeData b) => b -> INode -> IO BIndex
addINode context node' = 
  do let node = newINode (inodeDataPtr node') (inodeType node')
     Just node2 <- getINode context rootINodeIndex
     let (INodeRootDirectory inodeid dataid) = inodeType node2
         dataindex = inodeDataPtr node2
     putINode context (inodeid+1) $ newINode (inodeDataPtr node) (inodeType node)
     putINode context rootINodeIndex $ newINode dataindex (INodeRootDirectory (inodeid+1) dataid)
     return (inodeid + 1)

deleteINode :: (BTreeData b) => b -> BIndex -> IO ()
deleteINode context nodePtr = deleteRecord context (INodeIndex nodePtr)

putINodeData :: (BTreeData b) => b -> BIndex -> INodeData -> IO ()
putINodeData context n dataNode = putRecord context (INodeDataIndex n) (BINodeData dataNode)

getINodeData :: (BTreeData b) => b -> BIndex -> IO (Maybe INodeData)
getINodeData context n = do
   mnode <- getRecord context (BTreeIndex $ INodeDataIndex n)
   maybe (return Nothing) (\ (BINodeData node) -> return $ Just node) mnode

addINodeData :: (BTreeData b) => b -> INodeData -> IO BIndex
addINodeData context nodeData = do
   Just node <- getINode context rootINodeIndex
   let dataIndex = inodeDataPtr node
       (INodeRootDirectory ni di) = inodeType node
   putINodeData context (di+1) nodeData
   putINode context rootINodeIndex (newINode dataIndex (INodeRootDirectory ni (di+1)))
   return (di+1)

deleteINodeData :: (BTreeData b) => b -> BIndex -> IO ()
deleteINodeData context n = deleteRecord context (INodeDataIndex n)


putFile :: (BTreeData b) => b -> String -> INodeType -> INodeData -> IO ()
putFile context path inodeType inodeData = do
   let ppath = parentPath path
   mfolderNode <- pathToINode context ppath
   maybe (error "putFile: Incorrect parent path") 
      withFolderNode mfolderNode
   return ()
  where
      withFolderNode (INode dataIndex _) = do 
         mfolder <- getINodeData context dataIndex
         maybe (return ()) (withFolder dataIndex) mfolder
      
      withFolder folderIndex (Dir fileMap) = do
         let fileArr = breakn '/' path
             file = L.last fileArr
             mnodeindex = M.lookup file fileMap
         case mnodeindex of
           Nothing -> do 
               dataindex <- addINodeData context inodeData
               nodeindex <- addINode context (INode dataindex inodeType)
               let newFileMap = M.insert file nodeindex fileMap
               putINodeData context folderIndex (Dir newFileMap)
           Just nodeIndex -> do
               mnode <- getINode context nodeIndex
               maybe (error $ "putFile: Cannot find INode "++(show nodeIndex))
                      (putFile' nodeIndex) mnode
               
      putFile' nodeIndex node = do
         let dataIndex = inodeDataPtr node
         putINodeData context dataIndex inodeData
         putINode context nodeIndex (newINode dataIndex inodeType)

getFile :: (BTreeData b) => b -> String -> IO (Maybe File)
getFile context path = do
   mresult <- pathToINode context path
   maybe (return Nothing) 
      (\ inode@(INode di _) -> do
         mdata <- getINodeData context di
         maybe (return Nothing) 
            (return . Just . File path inode) mdata)
      mresult

addFile :: (BTreeData b) => b -> String -> INodeType -> INodeData -> IO ()
addFile context path inodeType inodeData = putFile context path inodeType inodeData

deleteFile :: (BTreeData b) => b -> String -> IO ()
deleteFile context path = do
   minodeIndex <- pathToINodeIndex context path
   maybe (error "deleteFile: inode index not found") withIndex minodeIndex
  where
    withIndex inodeIndex = do
      minode <- getINode context inodeIndex
      maybe (error "deleteFile: inode not found")
                  (withINode inodeIndex) minode

    withINode inodeIndex node = do 
      let dataIndex = inodeDataPtr node
      deleteINodeData context dataIndex
      deleteINode context inodeIndex
      let ppath = parentPath path      
      mparentINode <- pathToINode context ppath
      maybe (error "deleteFile: parent folder doesn't exist") 
         withFolderNode mparentINode

    withFolderNode (INode di _) = do
      mfolder <- getINodeData context di
      maybe (error "deleteFile: folder doesn't exist") (withFolder di) mfolder

    withFolder di (Dir folder) = do
      let file = L.last $ breakn '/' path
          newFolder = M.delete file folder
      putINodeData context di (Dir newFolder)


putDb context path db = 
   putFile context path INodeDatabase (Dir db)

addDb context path = 
   putFile context path INodeDatabase (Dir M.empty)

--
-- TODO: Delete tables within
--
deleteDb context path = deleteFile context path 

getTableNames context path = do
   Just file <- getFile context path
   let dirMap = inodeDir (fileData file)
       tables = M.keys dirMap
   return tables


putTable :: (BTreeData b) => b -> String -> BIndex -> TableSchema -> IO ()

putTable context path rowid schema = do
   let tableNode = INodeTable { inodeTableRowid = rowid, inodeTableSchema = schema }
   putFile context path tableNode (FileText "table")


getTable :: (BTreeData b) => b -> String -> IO (Maybe Table)

getTable context path = do
   Just inodeindex <- pathToINodeIndex context path
   Just node <- getINode context inodeindex
   let (INodeTable rowid schema) = inodeType node
   return $ Just $ newTable (inodeDataPtr node) path rowid schema
   

getLastRowid context path = do
   Just node <- pathToINode context path
   let (INodeTable lastRowid _) = inodeType node
   return lastRowid

-- 
-- TODO: delete table records.
--
deleteTable context path = deleteFile context path

putTableRow context path n tableRow = do
   Just node <- pathToINode context path
   let dataPtr = inodeDataPtr node
       (INodeTable rowid schema) = inodeType node
   case validate schema tableRow of
        True -> do 
          handleIndex context path dataPtr schema tableRow n
          putRecord context (TableIndex dataPtr n) (BTableRow tableRow)
        otherwise -> error "putTableRow: Not valid record"
  

getTableRow context path n = do
   Just inodeindex <- pathToINodeIndex context path
   Just node <- getINode context inodeindex
   let dataIndex = inodeDataPtr node
   mrec <- getRecord context (BTreeIndex (TableIndex dataIndex n))
   maybe (return Nothing) (return . Just . btableRow) mrec

getTableRows context tablePath minKey maxKey = do
   Just node <- pathToINode context tablePath
   let di = inodeDataPtr node
   records <- treeRange context 
      (BTreeIndex $ TableIndex di minKey) 
      (BTreeIndex $ TableIndex di maxKey)
   return $ map (\ brec -> 
                   let BTableRow tableRow = recordData brec
                       BTreeIndex (TableIndex _ index) = recordKey brec
                   in (index, tableRow)) records

getTableRowids context tablePath minKey maxKey = do
   recs <- getTableRows context tablePath minKey maxKey
   return $ map fst recs

addTableRowTrans :: (BTreeData b) => b -> String -> BIndex -> BIndex -> BIndex -> 
   TableSchema -> TableRow -> IO ()
addTableRowTrans context path rootIndex tableIndex rowid schema tableRow = do
   case validate schema tableRow of
        True -> doInsert
        otherwise -> error "addTableRowTrans: Record did not validate."
   return ()
   where
      doInsert = do
         handleIndex context path tableIndex schema tableRow rowid
         treeInsert context rootIndex (newBRecord (BTreeIndex $ TableIndex tableIndex rowid) 
            (BTableRow tableRow) (Just rootIndex))

addTableRows :: (BTreeData b) => b -> String -> [TableRow] -> IO ()
addTableRows context path rows = do
   Just index <- pathToINodeIndex context path
   Just node <- getINode context index
   let dataPtr = inodeDataPtr node
       (INodeTable rowid schema) = inodeType node
   root <- getMasterRoot context
   newRowid <- iterRows root schema dataPtr rowid rows
   putINode context index $ newINode dataPtr (INodeTable newRowid schema)
   where
      iterRows root schema dataPtr ri [] = return ri
      iterRows root schema dataPtr newRi (row:rows) = do
         addTableRowTrans context path root dataPtr (newRi+1) schema row
         iterRows root schema dataPtr (newRi+1) rows


addTableRow :: (BTreeData b) => b -> String -> TableRow -> IO ()
addTableRow context path tableRow = addTableRows context path [tableRow]

deleteTableRow :: (BTreeData b) => b -> String -> BIndex -> IO ()
deleteTableRow context path rowid = do
   Just index <- pathToINodeIndex context path
   Just node <- getINode context index
   mrow <- getTableRow context path rowid
   Just table <- getTable context path
   let dindex = inodeDataPtr node
       tIndex = tableIndex table
       schema = tableSchema table
       rowMap = maybe M.empty (\ (TableRow rm) -> rm) mrow
       indexFields = case schema of
                          EmptySchema -> []
                          otherwise -> M.keys $ tableIndexes schema
   doDelete' tIndex rowMap indexFields
   deleteRecord context (TableIndex dindex rowid)
  where
      doDelete' tIndex rowMap [] = return ()
      doDelete' tIndex rowMap (field:fields) = do
         let Just col = M.lookup field rowMap
         deleteTableRowIndex context path tIndex field col rowid
         doDelete' tIndex rowMap fields


deleteTableRowIndex :: (BTreeData b) => b -> String -> BIndex -> String -> TableColumn -> 
   BIndex -> IO ()
deleteTableRowIndex context path tableIndex field tableCol rowid = do
   rows <- getTableRowIndexes context path field tableCol
   case rows of
        [] -> return ()
        otherwise -> do
           Just node <- pathToINode context path
           let di = inodeDataPtr node
               nodeType = inodeType node
               rowids = L.delete rowid rows
           putRecord context (TableRowIndex di field tableCol)
               (BTableRow $ TableRow $ M.fromList [("rowid", TableRowids rowids)])


putTableRowIndex :: (BTreeData b) => b -> String -> BIndex -> String -> 
   TableColumn -> BIndex -> IO ()
putTableRowIndex context path tableIndex field tableCol rowid = do
   mrow <- getTableRow context path rowid
   maybe (return ()) (\ (TableRow rowMap) -> do
      let moldCol = M.lookup field rowMap
      case moldCol of
           Nothing -> return ()
           Just oldCol -> 
               if oldCol == tableCol
               then return ()
               else deleteTableRowIndex context path tableIndex field oldCol rowid) 
      mrow
   mrec <- getRecord context (BTreeIndex $ TableRowIndex tableIndex field tableCol)
   maybe (withoutRow tableIndex [rowid]) (withRow tableIndex) mrec 
  where
      withRow dataIndex (BTableRow (TableRow rowMap)) = do
         let mrowids = M.lookup "rowid" rowMap
             rowids = case mrowids of
                           Nothing -> []
                           Just (TableRowids rids) -> rids
             mexists = L.find (rowid==) rowids
             newRowids = maybe (rowid:rowids) (\_ -> rowids) mexists
         withoutRow dataIndex newRowids

      withoutRow dataIndex rowids = do
         putRecord context (TableRowIndex dataIndex field tableCol) 
            (BTableRow $ TableRow $ M.fromList [("rowid", TableRowids (L.nub rowids))])

getTableRowIndexes :: (BTreeData b) => b -> String -> String -> TableColumn -> IO [BIndex]
getTableRowIndexes context path field tableCol = do
   mnode <- pathToINode context path
   maybe (return []) withNode mnode
  where
    withNode node = do 
      let dataPtr = inodeDataPtr node
      mrow <- getRecord context (BTreeIndex $ TableRowIndex dataPtr field tableCol)
      maybe (return []) withRow mrow
    withRow (BTableRow (TableRow rowMap)) = 
      let mrowid = M.lookup "rowid" rowMap
      in maybe (return []) (\ (TableRowids rowids) -> return rowids) mrowid

getTableRowsByIndex context path field tableCol = do
   indexes <- getTableRowIndexes context path field tableCol
   mrows <- mapM (\ rowid -> do
      row <- getTableRow context path rowid
      return (rowid, row)) indexes
   return $ foldl (\ arr (rowid, mrow) -> 
      case mrow of
           Nothing -> arr
           Just row -> ((rowid,row):arr)) [] mrows
                  

handleIndex context path tableIndex EmptySchema tableRow rowid = return ()
handleIndex context path tableIndex schema (TableRow rowMap) rowid = do
   let fields = M.keys $ tableIndexes schema
   mapM forField fields
   return ()
  where
      forField field = do
         let mindex = M.lookup field (tableIndexes schema)
             mcolVal = M.lookup field rowMap
         case (mindex, mcolVal) of
               (Nothing, _) -> return ()
               (_, Just colVal) -> do 
                  putTableRowIndex context path tableIndex field colVal rowid

putIndexes :: (BTreeData b) => b -> String -> [String] -> IO ()
putIndexes context path indexes = do
   let indexMap = M.fromList $ map (\ index -> (index, True)) indexes
   Just table <- getTable context path
   let di = tableIndex table
       path = tablePath table
       rowid = tableRowid table
       schema = tableSchema table
       cols = case schema of
                   EmptySchema -> M.empty
                   TableSchema { 
                     tableColumns = cs, 
                     tableIndexes  = _ 
                   } -> cs
   putTable context path rowid (newTableSchema cols indexMap)
   return ()

reindex context path = do
   allrecords <- getAllRecords context path
   reindex' allrecords
   where
      reindex' [] = return ()
      reindex' ((index, row):rows) = do
         putTableRow context path index row
         reindex' rows

getAllRecords context path = do
   lastRow <- getLastRowid context path
   if (lastRow > 0)
      then getTableRows context path 1 lastRow
      else return []
  
rootDataIndex :: BIndex
rootDataIndex = 1 

rootINodeIndex :: BIndex
rootINodeIndex = 1

initFs :: (BTreeData b) => b -> IO ()
initFs context = do
  root <- getMasterRoot context
  putINode context rootINodeIndex 
    (newINode rootDataIndex (INodeRootDirectory rootINodeIndex rootDataIndex))
  putINodeData context rootDataIndex $ Dir M.empty

parentPath path = 
   let dirArr = breakn '/' path
       dirSize = L.length dirArr
       parentPath' = if dirSize == 1
                        then (1, "/")
                        else foldl (\ (i, newPath) str -> 
                           case i < dirSize of
                                True -> (i+1, newPath ++ "/" ++ str)
                                False -> (i, newPath)) (1, "") dirArr
       (_, result) = parentPath' 
   in result
  


-- Always returns true for now
validate _ _ = True

{- validate schema (TableRow recordData) = 
  and [isMapped, correctColCount]
  where
    mapped = map (\ (key, value) -> maybe False (getType value) (M.lookup key recordData)) 
             (M.toList colMap)
    colMap = tableColumns schema
    getType NumberField (TableNumber n) = True
    getType TextField (TableText str) = True
    getType RowIdField (TableRowid i) = True
    getType other value = False
    isMapped = foldl (\ result val -> 
                          if result == False  
                          then False
                          else val == True) True mapped
    correctColCount = (M.size $ tableColumns schema) == (M.size recordData) -}


pathToINodeIndex context tablePath = do
  let arr = breakn '/' tablePath
  mtuple <- recurPath context arr rootINodeIndex
  maybe (return Nothing) (return . Just . fst) mtuple

pathToINode context path = do
  dirArr <- return $ breakn '/' path
  mtuple <- recurPath context dirArr rootINodeIndex
  maybe (return Nothing) (return . Just . snd) mtuple

workerQueue ch = forever $ do
  f <- atomically $ readTChan ch
  f 
  return ()
  
recurPath :: (BTreeData b) => b -> [String] -> BIndex -> IO (Maybe (BIndex, INode))

recurPath context [] index = do  
  minode <- getRecord context (BTreeIndex $ INodeIndex index)
  maybe (return Nothing) (\ (BINode inode) -> return $ Just (index, inode)) minode
  
recurPath context (curDir:dirArr) inodeIndex = do
  mnode <- getRecord context (BTreeIndex $ INodeIndex inodeIndex)
  maybe (return Nothing) 
    (\ (BINode (INode dataIndex inodeType)) -> do
        mdir <- getRecord context (BTreeIndex $ INodeDataIndex dataIndex)
        maybe (return Nothing) withDir mdir) 
    mnode
  where
    withDir (BINodeData (Dir folder)) = do 
      let mnode = M.lookup curDir folder
      maybe (return Nothing) (recurPath context dirArr) mnode

deleteRecord context n = do
  root <- getMasterRoot context
  root <- treeDelete context root (newBRecord (BTreeIndex n) BNullData Nothing)
  putMasterRoot context root
  
putRecord context n node = do
  mnode <- getRecord context (BTreeIndex n)
  root <- getMasterRoot context
  root <- maybe (insert root) (update root) mnode
  putMasterRoot context root
    where
      update root _ = do record <- setupRecord root
                         treeUpdate context root record
                      
      insert root = treeInsert context root $ newBRecord (BTreeIndex n) node (Just root)
                         
      setupRecord root = return $ newBRecord (BTreeIndex n) node (Just root)

getRecord context n = do
  root <- getMasterRoot context
  mresult <- treeSearch context root $ newBSearch n
  maybe (return Nothing) (return . Just . recordData . brecordResult) mresult

