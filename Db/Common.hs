module Db.Common where

import qualified Data.List as L
import System.IO
import System.CPUTime
import Data.Char (isSpace)


type BIndex = Integer

trim :: String -> String
trim = f . f
   where f = reverse . dropWhile isSpace


breakn :: Eq a => a -> [a] -> [[a]]
breakn n [] = []
breakn n uri = result
   where
      val1 = L.takeWhile (/= n) uri
      val2 = L.dropWhile (/= n) uri
      val3 = if L.null val2 
                then val2
                else tail val2
      result = if L.null val1
                  then breakn n val3
                  else (val1 : breakn n val3)


wf :: FilePath -> (Handle -> IO r) -> IO r
wf file f = withFile file ReadWriteMode f


time f = do
  start <- getCPUTime
  f
  end <- getCPUTime
  diff <- return $ (fromIntegral (end - start)) / (10^12)
  print diff
