module Db.File where

import Db.Common
import Db.TreeIndex
import Db.Table
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import qualified Data.ByteString as B
import qualified Data.Map as M

data INode = INode { inodeDataPtr :: BIndex
                   , inodeType :: INodeType }
             deriving (Show, Eq, Ord)

newINode ptr ntype = INode ptr ntype

data INodeType = INodeFile
               | INodeDirectory
               | INodeDatabase
               | INodeTable { inodeTableRowid :: BIndex 
                            , inodeTableSchema :: TableSchema }
               | INodeRootDirectory { inodeRowid :: BIndex
                                    , dataRowid :: BIndex }
                 deriving (Show, Eq, Ord)

data INodeData = Dir (M.Map String BIndex)
               | FileData B.ByteString
               | FileText String
                 deriving (Show, Eq, Ord)


inodeDir (Dir dirMap) = dirMap


data File = File { filePath :: String
                 , fileINode :: INode
                 , fileData :: INodeData }
            deriving Show
                     

instance Serialize INodeData where
  put (Dir dir) = do
    putWord8 0
    put dir
  
  put (FileData bs) = do
    putWord8 1
    put bs
  
  put (FileText str) = do
    putWord8 2
    put str
  
  get = do
    dn <- getWord8
    case dn of
      0 -> do dir <- get :: Get (M.Map String BIndex)
              return $ Dir dir
      1 -> do bs <- get :: Get B.ByteString
              return $ FileData bs
      2 -> do str <- get :: Get String
              return $ FileText str
              

instance Serialize INodeType where
  put nt = do
    case nt of
      INodeFile -> putWord8 0
      INodeDirectory -> putWord8 1
      INodeDatabase -> putWord8 2
      INodeTable rowid ts -> do putWord8 3
                                put rowid
                                put ts
      INodeRootDirectory nodeid dataid -> do putWord8 4
                                             put nodeid
                                             put dataid
  
  get = do
    nt <- getWord8
    case nt of
      0 -> return INodeFile
      1 -> return INodeDirectory
      2 -> return INodeDatabase
      3 -> do rowid <- get :: Get BIndex
              ts <- get :: Get TableSchema
              return $ INodeTable rowid ts
      4 -> do nodeid <- get :: Get BIndex
              dataid <- get :: Get BIndex
              return $ INodeRootDirectory nodeid dataid

instance Serialize INode where
  put nt = do
    putWord8 0
    put $ inodeDataPtr nt
    put $ inodeType nt
  
  get = do
    nt <- getWord8
    case nt of
      0 -> do indata <- get :: Get BIndex
              intype <- get :: Get INodeType
              return $ INode indata intype
