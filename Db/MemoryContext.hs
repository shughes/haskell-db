module Db.MemoryContext 
    ( MemoryContext
    , memoryState
    , memoryChannel

    , initMemoryContext )
    where

import Db.BPage
import Db.Common
import Db.BTreeData
import Db.BRecord
import Db.BDataType
import Db.TreeIndex
import System.IO
import Control.Concurrent.STM
import qualified Data.Map as M
import qualified Data.List as L


data MemoryContext = MemoryContext { memoryState :: TVar (M.Map BIndex BPage, M.Map BIndex BPage) 
                                   , memoryChannel :: TChan (IO ()) }

instance BTreeData MemoryContext where
    putBPage context n page = putPageCache context n page
    getBPage context n = getPageCache context n
    getMaxBIndex context = getMaxBIndex' context 
    putMaxBIndex context n = putMaxBIndex' context n
    getMasterRoot context = getMasterRoot' context
    putMasterRoot context n = putMasterRoot' context n
    nextBIndex context  = nextBIndex' context 


managedMaxBPageMeta :: BIndex
managedMaxBPageMeta = (-1)

managedMasterRootMeta :: BIndex
managedMasterRootMeta = (-2)

initMemoryContext = do
  tvar <- newTVarIO (M.empty, M.empty)
  tchan <- newTChanIO
  context <- return $ MemoryContext tvar tchan
  return context
  

getMasterRoot' context = do
  result <- getPageCache context managedMasterRootMeta
  return $ maybe 0 getGeneric result

putMasterRoot' context n = 
   putPageCache context managedMasterRootMeta 
      (newBPage [newBRecord (BTreeIndex $ INodeDataIndex n) BNullData Nothing] [])
                           
getGeneric page = 
   let childs = bChildren page
       recs = bRecords page
   in case and [(L.length recs) == 1, (L.length childs) == 0] of
           True -> let rec = L.head recs
                       index = recordKey rec
                       rdata = recordData rec
                       rhist = recordHistory rec
                   in case (index, rdata, rhist) of
                           (BTreeIndex (INodeDataIndex master), BNullData, Nothing) -> master
                           otherwise -> 0
           otherwise -> 0


putPageCache context n page = do
  atomically $ do
    (pageMap, oldMap) <- readTVar (memoryState context)
    let newMap = M.insert n page pageMap
    writeTVar (memoryState context) (newMap, oldMap)

getPageCache context n = do
   (pageMap, oldMap) <- readTVarIO (memoryState context)
   mpage <- return $ M.lookup n pageMap
   case mpage of 
        Nothing -> return $ M.lookup n oldMap
        otherwise -> return mpage
  
getMaxBIndex' context = do
   result <- getPageCache context managedMaxBPageMeta
   return $ maybe 0 (\ r -> getGeneric r) result

putMaxBIndex' context n = 
   putPageCache context managedMaxBPageMeta 
      (newBPage [newBRecord (BTreeIndex $ INodeDataIndex n) BNullData Nothing] []) 
  
nextBIndex' context = atomically $ do
   (pageMap, oldMap) <- readTVar (memoryState context)
   mpage <- return $ M.lookup managedMaxBPageMeta pageMap
   let mmaxPage = maybe (M.lookup managedMaxBPageMeta oldMap) Just mpage
       max = maybe 0 getGeneric mmaxPage
       newMap = M.insert managedMaxBPageMeta 
         (newBPage [newBRecord (BTreeIndex $ INodeDataIndex (max+1)) 
         BNullData Nothing] [])
         pageMap
   writeTVar (memoryState context) (newMap, oldMap)
   return (max+1)
