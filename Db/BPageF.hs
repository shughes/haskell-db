module Db.BPageF where

import Db.BRecord
import Db.Common
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import qualified Data.ByteString as B


data BPageF = BPageTree { btRecords :: [BRecord]
                        , btChildren :: [BIndex] }
            | BPageOverflow B.ByteString BIndex
            | BPageVersionList [BIndex] 
            | BPageFreePtr BPageF BIndex BIndex
              deriving (Show, Eq, Ord)
               
                          
instance Serialize BPageF where
    put (BPageTree r c) = do 
      putWord8 0
      put r
      put c

    put (BPageOverflow bs bi) = do
      putWord8 1
      put bs
      put bi
    
    put (BPageVersionList lst) = do 
      putWord8 2
      put lst

    put (BPageFreePtr page lefti righti) = do
       putWord8 3
       put page
       put lefti
       put righti

    get = do
      kind <- getWord8
      case kind of
        0 -> do r <- get :: Get [BRecord]
                c <- get :: Get [BIndex]
                return $ BPageTree r c 
                
        1 -> do bs <- get :: Get B.ByteString
                bi <- get :: Get BIndex
                return $ BPageOverflow bs bi
                
        2 -> do lst <- get :: Get [BIndex]
                return $ BPageVersionList lst
                
        3 -> do page <- get :: Get BPageF
                lefti <- get :: Get BIndex
                righti <- get :: Get BIndex
                return $ BPageFreePtr page lefti righti
                
        otherwise -> return $ BPageTree [] []
