module Db.SQL where

import Data.Char
import Text.ParserCombinators.Parsec


--
-- type Parser a = GenParser Char () a
--
-- newtype GenParser tok st a = Parser (State tok st -> Consumed (Reply tok st a))
--
-- newtype State st a = State (st -> (a, st))
--
-- data Consumed a = Consumed a | Empty !a
--
-- data Reply tok st a = Ok !a !(State tok st) ParseError | Error ParseError
--
-- data State tok st = State { stateInput :: [tok], statePos :: !SourcePos, stateUser :: !st }
--
-- select key, val from map_table where key = 'value'
-- 

strToLower str = map toLower str

data Table = Table { tableName :: String
                   , tableVar :: String }
                     deriving Show

data Col = Col { colPrefix :: String
               , colName :: String }
                 deriving Show

data Var = StringVar String
         | IntVar Int
         | DoubleVar Double
           deriving Show

data SQL = SelectSQL [Table] [Col] [(Col, String, Var)]

select :: GenParser Char st SQL
select = do
   string "select"
   cols <- selectCols
   tables <- selectFrom <|> (return [])
   queries <- selectWhere <|> (return [])
   return $ SelectSQL tables cols queries

selectWhere = do
   spaces
   string "where"
   selectQueries

selectQueries = do
   query <- selectQuery
   arr <- (do char ','; selectQueries) <|> (return [])
   return (query:arr)

compareStr = do
   cmp1 <- char '=' <|> char '<' <|> char '>'
   cmp2 <- char '=' <|> (return ' ')
   cmp <- case cmp2 of
               ' ' -> return [cmp1]
               otherwise -> return (cmp1:[cmp2])
   return cmp
   
selectQuery = do
   spaces
   key <- selectCol
   spaces
   cmp <- compareStr
   spaces
   val <- selectVar
   return (key, cmp, val)

selectVar = selectString <|> selectNumber

selectString = do
   char '\''
   val <- many $ noneOf "'"
   char '\''
   return $ StringVar val

selectNumber = do
   num <- many $ digit
   (do char '.'
       num2 <- many $ digit
       return $ DoubleVar $ read (num ++ "." ++ num2)) <|> (return $ IntVar $ read num)

selectFrom = do
   string "from"
   selectTables

selectTables = do
   spaces
   table <- selectTable
   spaces
   rest <- (char ',' >> selectTables) <|> (return [])
   return (table:rest)

selectTable = do
   table <- many $ noneOf ",; "
   spaces
   var <- (try whereKeyword) <|> (many $ noneOf ",; ")
   return $ Table table var
   where
      whereKeyword = do
         str <- getInput
         string "where"
         setInput str
         return []

selectCol = do
   val <- many $ noneOf " ;,."
   val2 <- (do char '.'; many $ noneOf " ;,") <|> (return [])
   return $ Col val val2

selectCols = do
   spaces
   col <- selectCol
   spaces
   arr <- (do char ','; selectCols) <|> (return [])
   return (col:arr)

parseSQL input = parse select "(unknown)" input
