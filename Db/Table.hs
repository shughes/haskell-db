module Db.Table where

import Db.Common
import qualified Data.Map as M
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import Data.Typeable

data Table = Table { tableIndex :: BIndex,
                     tablePath :: String,
                     tableRowid :: BIndex,
                     tableSchema :: TableSchema }
             deriving (Show, Eq, Ord)

newTable i p r s = Table i p r s

data TableRow = TableRow (M.Map String TableColumn)
                deriving (Show, Eq, Ord)

newTableRow tmap = TableRow tmap

data TableSchema = TableSchema { tableColumns :: M.Map String FieldType 
                               , tableIndexes :: M.Map String Bool }
                 | EmptySchema
                   deriving (Show, Eq, Ord)

newTableSchema cols indexes = TableSchema cols indexes
                            
data FieldType = TextField
               | NumberField
               | RowIdField
                 deriving (Show, Eq, Ord)

data TableColumn = TableText String
                 | TableNumber BIndex
                 | TableRowid BIndex
                 | TableRowids [BIndex]
                   deriving (Show, Eq, Ord)


newTableText str = TableText str

instance Serialize TableColumn where
  put (TableNumber i) = do
    putWord8 0
    put i
  
  put (TableText str) = do
    putWord8 1
    put str
  
  put (TableRowid i) = do
    putWord8 2
    put i
  
  put (TableRowids arr) = do
    putWord8 3
    put arr

  get = do 
    col <- getWord8
    case col of
      0 -> do i <- get :: Get BIndex
              return $ TableNumber i
      1 -> do str <- get :: Get String
              return $ TableText str
      2 -> do i <- get :: Get BIndex
              return $ TableRowid i
      3 -> do arr <- get :: Get [BIndex]
              return $ TableRowids arr
                            
instance Typeable TableColumn where
   typeOf (TableNumber num) = typeOf num
   typeOf (TableText str) = typeOf str
   typeOf (TableRowid rowid) = typeOf rowid
   typeOf (TableRowids rowids) = typeOf1 rowids

instance Serialize FieldType where
  put TextField = putWord8 0   
  put NumberField = putWord8 1
  put RowIdField = putWord8 2
  get = 
    do kind <- getWord8
       case kind of
         0 -> return TextField
         1 -> return NumberField
         2 -> return RowIdField
                          
instance Serialize TableRow where
  put (TableRow cols) = do
    putWord8 0
    put cols
    
  get = do
    kind <- getWord8
    case kind of
      0 -> do cols <- get :: Get (M.Map String TableColumn)
              return $ TableRow cols


instance Typeable TableRow where
   typeOf (TableRow cols) = typeOf1 (M.toList cols)
   

instance Serialize TableSchema where
  put (TableSchema tc i) = do
    putWord8 0
    put tc
    put i
  
  put EmptySchema = do
    putWord8 1
  
  get = do
    check <- getWord8
    case check of
      0-> do
          tc <- get :: Get (M.Map String FieldType)
          i <- get :: Get (M.Map String Bool)
          return $ TableSchema tc i
      otherwise -> return EmptySchema
    
