module Db.ManagedContext 
    ( ManagedContext(..) 
    , initManagedContext )
    where

import Db.MemoryContext
import Db.FileContext
import Db.BPage
import Db.Common
import Db.BTreeData
import Db.BRecord
import Db.BDataType
import Control.Concurrent.STM
import System.Directory
import System.IO
import qualified Data.Map as M


data ManagedContext = ManagedContext { managedFile :: FileContext 
                                     , managedMemory :: MemoryContext 
                                     , managedFilePath :: String }

instance BTreeData ManagedContext where
    putBPage context n page = putBPage (managedMemory context) n page
    getBPage context n = getBPage' context n
    getMaxBIndex context = getMaxBIndex' context 
    putMaxBIndex context n = putMaxBIndex (managedMemory context) n
    getMasterRoot context = getMasterRoot' context
    putMasterRoot context n = putMasterRoot (managedMemory context) n
    treeFlush context = treeFlush' context
    treeCommit context = treeCommit' context
    treeRollback context = treeRollback' context

    nextBIndex context = do
      memMax <- nextBIndex (managedMemory context)
      case memMax == 1 of
         True -> do fileMax <- getMaxBIndex (managedFile context)
                    putMaxBIndex (managedMemory context) fileMax
                    nextBIndex (managedMemory context) 
         False -> return memMax



initManagedContext file = do
  exists <- doesFileExist file
  p <- case exists of
            True -> initFileContext file
            False -> newFileContext file
  m <- initMemoryContext
  return $ ManagedContext p m file

getBPage' :: ManagedContext -> BIndex -> IO (Maybe BPage)
getBPage' context n = do
  result <- getBPage (managedMemory context) n
  case result of
    Nothing -> do mpage <- getBPage (managedFile context) n
                  page <- return $ maybe (newBPage [] []) (\ bp -> bp) mpage
                  (pageMap, oldMap) <- readTVarIO $ memoryState $ managedMemory context
                  let oldMap2 = M.insert n page oldMap
                  atomically $ writeTVar (memoryState $ managedMemory context) (pageMap, oldMap2)
                  return $ Just page
    otherwise -> return result

getMasterRoot' :: ManagedContext -> IO BIndex
getMasterRoot' context = do
  result <- getMasterRoot (managedMemory context)
  case result of
    0 -> getMasterRoot (managedFile context) 
    otherwise -> return result

getMaxBIndex' :: ManagedContext -> IO BIndex
getMaxBIndex' context = do
  result <- getMaxBIndex (managedMemory context)
  case result of
       0 -> getMaxBIndex (managedFile context)
       otherwise -> return result

treeFlush' :: ManagedContext -> IO ()
treeFlush' context = do
  root <- getMasterRoot context
  Just rootPage <- getBPage context root
  stmState <- return $ memoryState $ managedMemory context
  (pageMap, oldmap) <- readTVarIO stmState
  case M.size pageMap of
    0 -> return ()
    otherwise -> do
      oldRoot <- getMasterRoot $ managedFile context
      oldMax <- getMaxBIndex $ managedFile context
      max <- getMaxBIndex context
      let (newmax, newroot, result) = getNewIndexes root max $ M.toList pageMap
      fileContext <- return $ managedFile context
      putMaxBIndex fileContext newmax
      savePages fileContext result
      let state = memoryState $ managedMemory context
      atomically $ writeTVar state (M.empty, M.empty) --M.fromList result)
      putMasterRoot context newroot
      return ()

treeCommit' context = do
  treeFlush context
  root <- getMasterRoot context
  putMasterRoot (managedFile context) root

treeRollback' context = do
  let state = memoryState $ managedMemory context
  atomically $ writeTVar state (M.empty, M.empty)
