module Db.BDataType where

import Db.Common
import Db.File
import Db.TreeIndex
import Db.Table
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put
import Control.Exception
import qualified Data.ByteString as B


data BDataType = BNullData 
               | BIndexData BIndex
               | BString String
               | BTreeIndex TreeIndex
               | BINode INode
               | BINodeData INodeData
               | BTableRow { btableRow :: TableRow }
                 deriving (Show, Eq, Ord)
  

instance Serialize BDataType where
    put (BIndexData i) = do
      putWord8 0
      put i
    
    put (BString s) = do
      putWord8 1
      put s
    
    put (BTableRow row) = do
      putWord8 2
      put row
      
    put BNullData = putWord8 5
       
    put (BTreeIndex i) = do
      putWord8 7
      put i
    
    put (BINodeData dir) = do
      putWord8 8
      put dir
    
    put (BINode inode) = do
      putWord8 9
      put inode

    get = do
      kind <- getWord8
      case kind of
        0 -> do i <- get :: Get BIndex
                return $ BIndexData i
                
        1 -> do s <- get :: Get String
                return $ BString s
                
        2 -> do row <- get :: Get TableRow
                return $ BTableRow row
                
        5 -> return BNullData
        
        7 -> do i <- get :: Get TreeIndex
                return $ BTreeIndex i
        
        8 -> do dir <- get :: Get INodeData
                return $ BINodeData dir
        
        9 -> do inode <- get :: Get INode
                return $ BINode inode
                
        otherwise -> throw $ NoMethodError (show kind)

