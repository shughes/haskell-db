module Db.BRecord 
   ( BRecord
   , newBRecord
   , newBSearch 
   , isBSearch
   , recordKey
   , recordData
   , recordHistory ) where 

import Db.Common
import Db.BDataType
import Data.Serialize
import Data.Serialize.Get
import Data.Serialize.Put

data BRecord = BRecord { recordKey :: BDataType
                       , recordData :: BDataType 
                       , recordHistory :: Maybe BIndex }
             | BSearch { recordKey :: BDataType }
               deriving (Show)

newBRecord key d history = BRecord key d history

isBSearch (BSearch _) = True
isBSearch other = False

newBSearch d = BSearch d

instance Eq BRecord where
    (BRecord key1 _ _) == (BRecord key2 _ _) = key1 == key2
    b1 /= b2 = (b1 == b2) == False


instance Ord BRecord where
   (BRecord key1 _ _) <= (BRecord key2 _ _) = key1 <= key2


instance Serialize BRecord where
    put (BRecord bk bd bi) = 
        do putWord8 0
           put bk
           put bd
           put bi
    
    get = do kind <- getWord8
             case kind of
               0 -> do bk <- get :: Get BDataType
                       bd <- get :: Get BDataType
                       bi <- get :: Get (Maybe BIndex)
                       return $ BRecord bk bd bi
