module Db.Main where

import Db.TableColumn
import Db.Common
import Db.FileContext
import Db.MemoryContext
import Db.ManagedContext
import Db.BTreeData
import Db.BPage
import Db.BRecord
import Db.BDataType
import Db.INode
import Db.INodeData
import Db.TreeIndex
import Db.Fs
import System.UUID.V4
import Text.JSON
import Data.Typeable
import System.IO
import Control.Concurrent.STM
import Control.Concurrent
import Control.Monad
import CPUTime
import qualified Data.ByteString as B
import qualified Data.Map as M
import qualified Data.List as L

-- m a -> (a -> m b) -> m b
-- a -> m a


{-
data State s a = State { runState :: (s -> (a, s)) }

put st = State $ \ s -> ((), st)

get = State $ \ s -> (s, s)

evalState act = fst . runState act

execState act = snd . runState act

pop = State $ \ (x:xs) -> (x, xs)

push val = State $ \ xs -> ((), val:xs)

testState = do
  result <- return $ evalState (do push 50
                                   push 100
                                   cur <- get
                                   put (10:cur)
                                   get) []
  print result
  result <- return $ execState (push 35 >>= \ _ ->
                                get >>= \ state ->
                                return state) result
  print result
  
      
instance Monad (State s) where
  st >>= f = State $ \ s -> let (a1, s1) = runState st s
                            in runState (f a1) s1
                               
  return a = State $ \ s -> (a, s)

-}

test path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- getINodeIndex context path
  hClose hdl
  return result


testDeleteFile path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  deleteFile context path
  treeFlush context
  hClose hdl

testUserTable = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  initFs context
  putDatabase context "/db" (Dir M.empty)
  putTable context "/db/users" EmptySchema
  
testGetTableRecords path min max = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- getTableRecords context path min max
  hClose hdl
  return result
  
testGetSchema path = do
  context <- initManagedContext testFile 
  hdl <- getHdl context
  result <- getTableSchema context path
  hClose hdl
  return result

genUUID :: IO String
genUUID = do
   u <- uuid
   id1 <- return $ show u
   let id = L.filter (\c -> c /= '-') id1
   return id

testDelete rec = do
  context <- initManagedContext testFile
  root <- getMasterRoot context
  treeDelete context root rec
  treeFlush context
  hClose $ fileHandle $ managedFile context
  
testBulkBenchmark arr = do
  context <- initManagedContext testFile
  root <- getMasterRoot context
  max <- getMaxBIndex context
  t1 <- getCPUTime
  root <- foldl (bulkInsert context root) (return root) arr
  --putMasterRoot context root
  treeFlush context
  t2 <- getCPUTime
  hClose $ fileHandle $ managedFile context
  return (div (t2 - t1) 1000000000000)
  where 
    bulkInsert context root r i = do 
      ri <- r
      newri <- case mod i 85000 of
                 0 -> do treeFlush context
                         getMasterRoot context
                 otherwise -> return ri
      --val <- genUUID
      treeInsert context newri (BRecord (BIndexData i) BNullData (Just root))


testBulkCreate arr = do
  let path = "/db/mytable"
  context <- initManagedContext testFile 
  initFs context
  putDatabase context "/db" (Dir M.empty)
  putTable context path EmptySchema --(TableSchema TableSchemaType $ M.fromList [("key", "number")])
  let chan = memoryChannel $ managedMemory context
  --tid <- forkIO $ workerQueue chan
  t1 <- getCPUTime
  let rowid = (L.head arr) - 1
  bulkInsert context path arr rowid
  treeFlush context
  hClose $ fileHandle $ managedFile context
  t2 <- getCPUTime
  --atomically $ writeTChan chan (killThread tid)
  return (div (t2 - t1) 1000000000000)
                                        

bulkInsert context path arr rowid = do
  Just inodeindex <- getINodeIndex context path
  Just (INode dataindex (INodeTable _ schema)) <- getINode context inodeindex
  let chan = memoryChannel $ managedMemory context
  root <- getMasterRoot context
  result <- foldl (\ rid n -> do 
                      ri <- rid
                      (asyncInsert root schema n dataindex ri)
                      return (ri+1))
            (return rowid)
            arr
  newRowid <- return result
  putINode context inodeindex (INode dataindex (INodeTable newRowid schema))
  return ()
  where
    asyncInsert root schema n di ri = do
      case mod n 25000 of
        0 -> treeFlush context
        otherwise -> return ()
      addTableRecord context root di (ri+1) (FileText (show (ri+1))) schema
      --bulkPutTableRecord context di (ri+1) (FileText (show (ri+1))) schema
        --(TableRecord $ M.fromList [("key", TableNumber (ri+1))]) schema

testGetRoot = do
  context <- initManagedContext testFile 
  hdl <- getHdl context  
  root <- getMasterRoot context
  hClose hdl
  return root

testGetFile path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  file <- getFile context path
  hClose hdl
  return file

testPutDatabase path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- putDatabase context path (Dir M.empty)
  treeFlush context
  hClose $ fileHandle $ managedFile context

testPutTable path schema = do
  context <- initManagedContext testFile
  result <- putTable context path schema
  treeFlush context
  hClose $ fileHandle $ managedFile context

testPutTableIndex path index rowid = do
  context <- initManagedContext testFile
  putTableIndexRecord context path index rowid
  treeFlush context
  hClose $ fileHandle $ managedFile context

testGetTableIndex path index = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- getTableIndexRecord context path index
  treeFlush context
  hClose $ fileHandle $ managedFile context
  return result

testPutTableRecord path n fileData = do
  context <- initManagedContext testFile
  putTableRecord context path n fileData
  treeFlush context
  hClose $ fileHandle $ managedFile context


testCreateTableRecord path fileData = do
  context <- initManagedContext testFile
  result <- createTableRecord context path fileData
  treeFlush context
  hClose $ fileHandle $ managedFile context

testGetTableRecord path n = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- getTableRecord context path n
  treeFlush context
  hClose $ fileHandle $ managedFile context
  return result

testGetPage n = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- getBPage context n
  hClose hdl
  return result

testPutFile path fileData = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  result <- putFile context path fileData
  treeFlush context
  hClose hdl
  return result

testInitFs = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  initFs context
  treeFlush context
  hClose $ fileHandle $ managedFile context

testFs = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  putINode context 2 (INode 2 INodeFile)
  putData context 2 (FileText "this is a test file.")
  treeFlush context
  hClose hdl

testGetINode n = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  inode <- getINode context n
  hClose hdl
  return inode

testGetData n = do
  context <- initManagedContext testFile
  getData context n

testCreateData = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  new <- createData context (FileText "test data")
  treeFlush context
  hClose hdl
  return new

testGetFolder path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  f <- getFolder context path
  hClose hdl
  return f

testCreateFolder path = do
  context <- initManagedContext testFile
  hdl <- getHdl context
  createFolder context path
  treeFlush context
  hClose hdl
  

getHdl context = do
  return $ fileHandle $ managedFile context


testFile = "test.db"

testSearch n = do
  context <- initFileContext testFile
  root <- getMasterRoot context
  treeSearch context root n

testRoot = do
  context <- initFileContext testFile
  getMasterRoot context

testPage n = do
  context <- initFileContext testFile
  getBPage context n

