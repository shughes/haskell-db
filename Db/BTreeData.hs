module Db.BTreeData 
    ( treeInsert
    , treeSearch
    , treeDelete
    , treeUpdate
    , treeRange
    , savePages
    , deflateTree
    , findPage
    , getNewIndexes
    , BTreeData(..) 
    , BResult(..) )
    where

import Db.Common
import Db.BRecord
import Db.BPage
import Db.BDataType
import Control.Exception
import Control.Concurrent.STM
import qualified Data.List as L
import qualified Data.Map as M

class BTreeData b where
    putBPage :: b -> BIndex -> BPage -> IO ()
    getBPage :: b -> BIndex -> IO (Maybe BPage)

    getMaxBIndex :: b -> IO BIndex
    putMaxBIndex :: b -> BIndex -> IO ()

    nextBIndex :: b -> IO BIndex

    putMasterRoot :: b -> BIndex -> IO ()
    getMasterRoot :: b -> IO BIndex

    treeCommit :: b -> IO ()
    treeCommit context = return ()

    treeRollback :: b -> IO ()
    treeRollback context = return ()

    treeFlush :: b -> IO ()
    treeFlush context = return ()


-- Must be odd number
orderSize :: Int
orderSize = 25

minNodes :: Int
minNodes = minChildren - 1

minChildren :: Int
minChildren = ceiling (orderSizeDouble / 2)

orderSizeDouble :: Double
orderSizeDouble = fromIntegral orderSize

reduceCommitMap :: BIndex -> BPage -> M.Map BIndex BPage -> [(BIndex, BPage)]
reduceCommitMap index page stMap = 
   let mapIndexes ci = maybe [] (withIndex ci) $ M.lookup ci stMap
       rcm p = concat $ map mapIndexes (bChildren p)
       withIndex ci bp = [(ci,bp)]++(rcm bp)
   in [(index, page)] ++ (rcm page)
   

deflateTree :: BIndex -> BIndex -> BIndex -> BPage -> M.Map BIndex BPage -> 
   (BIndex, BIndex, [(BIndex, BPage)])
deflateTree oldRoot oldMax rootIndex rootPage pageMap = 
   let reduced = reduceCommitMap rootIndex rootPage pageMap
   in getNewIndexesWithOld oldRoot rootIndex oldMax reduced
 

treeInsert :: (BTreeData b) => b -> BIndex -> BRecord -> IO BIndex
treeInsert context rootIndex record = do
   (pages, buffer, path) <- initInsert context rootIndex record
   {- case buffer of
     [] -> return ()
     otherwise -> do 
       let max = L.head buffer
       putMaxBIndex context max -}
   updateTree context rootIndex pages path


initInsert :: (BTreeData b) => b -> BIndex -> BRecord -> 
   IO ([(BIndex, BPage)], [BIndex], [(BIndex, BPage)])
initInsert context rootIndex record = do
   (index, parents, page') <- findPage context record rootIndex []
   max <- getMaxBIndex context
   page <- return $ newBPage (L.insert record (bRecords page')) []
   (pages, buffer) <- performRefactor context 
                         ((length (bRecords page)) == orderSize) 
                         index parents page [max]

   case (isUnique record page') of
        True -> return (pages, buffer, parents)
        False -> error "initInsert: Not unique"


performRefactor :: (BTreeData b) => b -> Bool -> BIndex -> [(BIndex, BPage)] -> BPage -> 
                   [BIndex] -> IO ([(BIndex, BPage)], [BIndex])

performRefactor context False index path page buffer = return ([(index, page)], buffer)

performRefactor context True index ((pIndex, parent):parents) page buffer = do 
  (nextIndex, newBuf) <- findNextIndex context buffer
  newParent <- return $ getNewParent nextIndex
  lst <- return [(index, newLeft nextIndex), (nextIndex, newRight)]
  (rest, newBuf2) <- performRefactor context ((length newPRecords)==orderSize) 
                     pIndex parents newParent newBuf
  return ((lst ++ rest), newBuf2)
    where
      (promote, (left, right), (lc, rc)) = splitPage page
      getNewParent nextIndex = newBPage newPRecords (newPChildren nextIndex)
      newLeft nindex = newBPage left lc
      newRight = newBPage right rc
      newPChildren nextIndex = newChildren nextIndex
      newPIndex = findIndex promote (bRecords parent)
      newPRecords = let (l, r) = splitAt newPIndex (bRecords parent)
                    in (l++[promote]++r)
      tempC newIndex = map (\c -> if c == index
                                  then [index, newIndex]
                                  else [c]) (bChildren parent)
      newChildren newIndex = foldl (\a b -> a ++ b) [] (tempC newIndex)
      
performRefactor context True index [] page buffer = 
   let (promote, (left, right), (lc, rc)) = splitPage page
       leftPage nindex = newBPage left lc
       rightPage = newBPage right rc
       rootPage leftIndex rightIndex = newBPage [promote] [leftIndex, rightIndex]
   in do
      (leftIndex, newBuf) <- findNextIndex context buffer
      (rightIndex, newBuf2) <- findNextIndex context newBuf
      return ([(index, (rootPage leftIndex rightIndex)),
         (leftIndex, leftPage rightIndex), (rightIndex, rightPage)], newBuf2)


isUnique :: BRecord -> BPage -> Bool
isUnique newRecord page = 
   let records = bRecords page
   in foldl isUnique' True records
   where
      isUnique' is record = 
         case is of
              False -> False
              True -> record /= newRecord



findNextIndex :: (BTreeData b) => b -> [BIndex] -> IO (BIndex, [BIndex])
findNextIndex context buffer = do
   max <- nextBIndex context
   return (max, [])

{-
   maxBuff <- case length buffer of
                   0 -> return 0
                   otherwise -> return $ L.maximum buffer
   return (maxBuff+1, (maxBuff+1:buffer))
-}
   


updateTree :: (BTreeData b) => b -> BIndex -> [(BIndex, BPage)] -> [(BIndex, BPage)] -> IO BIndex
updateTree context rootIndex pages path  = do
  mergedPages <- return $ getMergedPages pages path
  savePages context mergedPages
  return rootIndex


findPage :: (BTreeData b) => b -> BRecord -> BIndex -> [(BIndex, BPage)] -> 
            IO (BIndex, [(BIndex, BPage)], BPage)

findPage context nr index parents = do 
   Just page <- getBPage context index
   case (isLeaf page) of
        True -> do return (index, parents, page)
        False -> do idx <- return $ findIndex nr (bRecords page)
                    findPage context nr ((bChildren page) !! idx) ((index, page):parents)
                 

splitPage :: BPage -> (BRecord, ([BRecord], [BRecord]), ([BIndex], [BIndex]))

splitPage page = (toPromote, leftRight, children)
    where
      rs = bRecords page
      n = (length rs) `div` 2
      children = handleChildren $ bChildren page
      leftRight = handleLeaf (isLeaf page)
      toPromote' = rs !! n
      toPromote = newBRecord (recordKey toPromote') BNullData (recordHistory toPromote')
      handleLeaf True = splitAt n rs
      handleLeaf False = let rs' = map (\r -> newBRecord (recordKey r) 
                                 BNullData (recordHistory r)) rs
                             (l,r) = splitAt n rs'
                         in (l, tail r)
      handleChildren [] = ([], [])
      handleChildren children = 
          let cn = (length children) `div` 2
              (lc, rc) = splitAt cn children
          in (lc,rc)


findIndex :: BRecord -> [BRecord] -> Int
findIndex nr [] = 0
findIndex nr (r:rs) = 
    if (recordKey nr) < (recordKey r)
    then 0
    else 1 + (findIndex nr rs)

getMergedPages :: [(BIndex, BPage)] -> [(BIndex, BPage)] -> [(BIndex, BPage)]
getMergedPages left right = L.unionBy (\ (x,_) (y,_) -> x==y) left right


getNewIndexesWithOld :: BIndex -> BIndex -> BIndex -> [(BIndex, BPage)] -> 
   (BIndex, BIndex, [(BIndex, BPage)])
getNewIndexesWithOld oldRootIndex rootIndex max oldPages = 
   withNewIndexes oldRootIndex rootIndex max max M.empty [] oldPages
   

savePages :: (BTreeData b) => b -> [(BIndex, BPage)] -> IO ()
savePages context [] = return ()
savePages context ((index, page):pages) = do 
  case index >= 0 of
    True -> putBPage context index page
    False -> return ()
  savePages context pages


isLeaf :: BPage -> Bool
isLeaf page = 
    case (bChildren page) of 
      [] -> True
      otherwise -> False

adjustChildren :: BIndex -> M.Map BIndex BIndex -> [(BIndex, BPage)] -> [(BIndex, BPage)]

adjustChildren rootIndex mapping [] = []

adjustChildren rootIndex mapping ((index, page) : pages) = 
   ((index, newBPage newRecords newChildren) : adjustChildren rootIndex mapping pages)
   where
      children = bChildren page
      adjustChild child = maybe child id (M.lookup child mapping)
      newChildren = map adjustChild children
      adjustRecord brec = newBRecord (recordKey brec) (recordData brec) $ Just rootIndex
      newRecords = map adjustRecord (bRecords page)


withNewIndexes :: BIndex -> BIndex -> BIndex -> BIndex -> M.Map BIndex BIndex -> 
   [(BIndex, BPage)] -> [(BIndex, BPage)] -> (BIndex, BIndex, [(BIndex, BPage)])

withNewIndexes oldRootIndex rootIndex max i mapping pages [] = 
   (i, newRoot, newPages)
   where
      newRoot = maybe rootIndex id (M.lookup rootIndex mapping)
      newPages = adjustChildren oldRootIndex mapping pages

withNewIndexes oldRootIndex rootIndex max i mapping newPages ((oldIndex, page) : rest) = 
   withNewIndexes oldRootIndex rootIndex max (i+1) 
      (M.insert oldIndex (i+1) mapping) ((i+1, page) : newPages) rest

getNewIndexes :: BIndex -> BIndex -> [(BIndex, BPage)] -> (BIndex, BIndex, [(BIndex, BPage)])
getNewIndexes rootIndex max oldPages = 
   withNewIndexes rootIndex rootIndex max max M.empty [] oldPages


treeDelete :: (BTreeData b) => b -> BIndex -> BRecord -> IO BIndex
treeDelete context rootIndex record
   | (isBSearch record) == True = 
      treeDelete context rootIndex (newBRecord (recordKey record) BNullData Nothing)
   | otherwise = do
      (index, parents, page) <- findPage context record rootIndex []
      maybeRecord <- return $ findRecord record page
      newPage <- return $ continueDelete maybeRecord page
      result1 <- refactorDelete context index newPage parents
      result <- return $ prepareOldPagesD result1
      mergedPages <- return $ getMergedPages result parents
      savePages context mergedPages
      return rootIndex
      where
         continueDelete (Just r) page = removeRecord r page
         continueDelete Nothing page = page

findRecord :: BRecord -> BPage -> Maybe BRecord
findRecord record page = 
    foldl (\x y -> if y == record
                   then (Just y)
                   else x) Nothing (bRecords page)



refactorDelete :: (BTreeData b) => b -> BIndex -> BPage -> [(BIndex, BPage)] -> 
                  IO [(Maybe FullPage, Maybe FullPage, Maybe FullPage)]

refactorDelete context index page parents = do
  case needsRefactor page of
       True -> case isLeaf page of
                 True -> case parents of
                           [] -> return [(Just $ FullPage index page, Nothing, Nothing)]
                           otherwise -> refactorDeleteLeaf context index page parents
                 False -> refactorDeleteIndex context index page parents
       False -> do return [(Just $ FullPage index page, Nothing, Nothing)]
  where
    needsRefactor p = (length $ bRecords p) < minNodes


refactorDeleteIndex :: (BTreeData b) => b -> BIndex -> BPage -> [(BIndex, BPage)] -> 
                       IO [(Maybe FullPage, Maybe FullPage, Maybe FullPage)]

refactorDeleteIndex context index page ((pIndex, pPage):parents) = do
  Just sibling <- findSibling context index pPage
  (FullPage newIndex newPage, FullPage newPIndex newParent, newSibling) 
      <- case canBorrow sibling of
           True -> return $ borrowForIndex (FullPage index page) 
                   (FullPage pIndex pPage) sibling
           False -> return $ mergeForIndex (FullPage index page) 
                    (FullPage pIndex pPage) sibling
  (newIndex2, newParent2) <- case (length $ bRecords newParent) == 0 of
                               True -> return (newPIndex, DeletePage newIndex newParent)
                               False -> return (newIndex, FullPage newPIndex newParent)
  let arr = [(Just $ FullPage newIndex2 newPage, Just newParent2, Just newSibling)]
  result <- case parents of
              [] -> return []
              otherwise -> refactorDelete context newPIndex newParent parents 
  return $ arr ++ result


refactorDeleteLeaf :: (BTreeData b) => b -> BIndex -> BPage -> [(BIndex, BPage)] -> 
                      IO [(Maybe FullPage, Maybe FullPage, Maybe FullPage)]
refactorDeleteLeaf context index page ((pIndex, pPage):parents) = do
   Just sibling <- findSibling context index pPage
   (newPage, (FullPage newPIndex newParent), newSibling) 
       <- case canBorrow sibling of
            True -> return $ borrowForLeaf (FullPage index page) 
                    (FullPage pIndex pPage) sibling
            False -> return $ mergeForLeaf (FullPage index page) 
                     (FullPage pIndex pPage) sibling
   let arr = [(Just newPage, Just $ FullPage newPIndex newParent, Just newSibling)]
   result <- case parents of 
               [] -> return []
               otherwise -> refactorDelete context newPIndex newParent parents 
   return $ arr ++ result

prepareOldPagesD :: [(Maybe FullPage, Maybe FullPage, Maybe FullPage)] -> [(BIndex, BPage)]
prepareOldPagesD [] = []
prepareOldPagesD ((p1, p2, p3):rest) = 
    arr ++ (prepareOldPagesD rest)
    where
      toTuple (Just (FullPage index page)) = [(index, page)]
      toTuple (Just (DeletePage index page)) = []
      toTuple Nothing = []
      arr = (toTuple p1) ++ (toTuple p2) ++ (toTuple p3)


removeRecord :: BRecord -> BPage -> BPage
removeRecord record page = newBPage newRecords (newChildren indexOfRecord)
    where
      newRecords = L.delete record (bRecords page)
      indexOfRecord = L.findIndex (\x -> x == record) (bRecords page)
      newChildren (Just idx) = L.delete ((bChildren page) !! (idx+1)) (bChildren page)
      newChildren Nothing = (bChildren page)

data FullPage = FullPage BIndex BPage 
              | DeletePage BIndex BPage 
              | Sibling BIndex BPage Sibling 
                deriving Show

findSibling :: (BTreeData b) => b -> BIndex -> BPage -> IO (Maybe FullPage)
findSibling context index parent = 
    do
      page1 <- findSiblingPage context LeftSibling index parent
      page2 <- findSiblingPage context RightSibling index parent
      sibPage <- case (page1, page2) of
                   (Nothing, Nothing) -> return Nothing
                   (Just p1, Just p2) -> case (length $ bRecords p2) > 
                                              (length $ bRecords p1) of
                                           True -> return $ Just p2
                                           False -> return $ Just p1
                   (Nothing, Just p2) -> return $ Just p2
                   (Just p1, Nothing) -> return $ Just p1
      (sibIndex, leftOrRight) <- case sibPage == page1 of
                                   True -> return $ (findLeftIndex index parent, LeftSibling)
                                   False -> return $ (findRightIndex index parent, 
                                                      RightSibling)
      return $ retVal sibIndex sibPage leftOrRight
    where
      retVal (Just idx) (Just page) sibling = Just (Sibling idx page sibling)
      retVal _ _ _ = Nothing

findSiblingPage :: (BTreeData b) => b -> Sibling -> BIndex -> BPage -> IO (Maybe BPage)
findSiblingPage context sibling index page = 
    do
      case (idx sibling) of
        Just i -> getBPage context i
        Nothing -> return Nothing
    where
      idx RightSibling = findRightIndex index page
      idx LeftSibling = findLeftIndex index page
      

canBorrow :: FullPage -> Bool
canBorrow (Sibling index page sibType) = (length $ bRecords page) > minNodes


mergeForLeaf :: FullPage -> FullPage -> FullPage -> (FullPage, FullPage, FullPage)
mergeForLeaf (FullPage index page) (FullPage pIndex parent) 
                 (Sibling sibIndex sibPage RightSibling) = 
    (FullPage index newPage, FullPage pIndex newParent, DeletePage sibIndex sibPage)
    where
      newPage = newBPage ((bRecords page)++(bRecords sibPage)) (bChildren page) 
      (Just inbetweenIndex) = findInbetween index parent
      newParent = removeRecord ((bRecords parent) !! inbetweenIndex) parent
mergeForLeaf (FullPage index page) (FullPage pIndex parent) 
      (Sibling sibIndex sibPage LeftSibling) = 
   (FullPage sibIndex newPage, FullPage pIndex newParent, DeletePage index sibPage)
   where
      newPage = newBPage ((bRecords sibPage)++(bRecords page)) (bChildren page)
      (Just inbetweenIndex) = findInbetween sibIndex parent
      newParent = removeRecord ((bRecords parent) !! inbetweenIndex) parent


borrowForLeaf :: FullPage -> FullPage -> FullPage -> (FullPage, FullPage, FullPage)
borrowForLeaf (FullPage index page) (FullPage pIndex parent) 
                  (Sibling sibIndex sibPage RightSibling) = 
    (FullPage index newPage, FullPage pIndex newParent, FullPage sibIndex newSib)
    where
      sibRecord = head $ (bRecords sibPage)
      newPage = newBPage ((bRecords page)++[sibRecord]) (bChildren page)
      newSib = newBPage (tail (bRecords sibPage)) (bChildren sibPage)
      (Just inbetweenIndex) = findInbetween index parent
      newParentRecord = newBRecord (recordKey $ head $ bRecords newSib) BNullData
         (recordHistory $ head $ bRecords newSib)
      (leftPar, rightPar) = splitAt inbetweenIndex (bRecords parent)
      newRightPar = case null rightPar of
                      True -> []
                      False -> tail rightPar
      newParent = newBPage (leftPar ++ [newParentRecord] ++ newRightPar) 
                  (bChildren parent)
borrowForLeaf (FullPage index page) (FullPage pIndex parent) 
                  (Sibling sibIndex sibPage LeftSibling) =
    (FullPage index newPage, FullPage pIndex newParent, FullPage sibIndex newSib)
    where 
      sibRecord = last (bRecords sibPage)
      newPage = newBPage ([sibRecord] ++ (bRecords page)) (bChildren page)
      (newSibRecs, oldSibRecs) = splitAt ((length (bRecords sibPage)) - 1) (bRecords sibPage)
      newSib = newBPage newSibRecs (bChildren sibPage)
      (Just inbetweenIndex) = findInbetween sibIndex parent
      newParentRecord = newBRecord (recordKey $ head $ bRecords newPage) BNullData
         (recordHistory $ head $ bRecords newSib)
      (leftPar, rightPar) = splitAt inbetweenIndex (bRecords parent)
      newRightPar = case null rightPar of
                      True -> []
                      False -> tail rightPar
      newParent = newBPage (leftPar ++ [newParentRecord] ++ newRightPar) (bChildren parent) 


data Sibling = LeftSibling | RightSibling deriving Show

findRightIndex :: BIndex -> BPage -> Maybe BIndex
findRightIndex index page = findSiblingIndex RightSibling index page

findLeftIndex :: BIndex -> BPage -> Maybe BIndex
findLeftIndex index page = findSiblingIndex LeftSibling index page

findInbetween :: BIndex -> BPage -> Maybe Int
findInbetween left parent = index
    where
      children = bChildren parent
      index = L.findIndex (\x -> x == left) children

borrowForIndex :: FullPage -> FullPage -> FullPage -> (FullPage, FullPage, FullPage)
borrowForIndex (FullPage index page) (FullPage pIndex parent) 
                   (Sibling sibIndex sibPage LeftSibling) = 
    (FullPage index newPage, FullPage pIndex newParent, FullPage sibIndex newSib)
    where
      Just inbetweenIndex = findInbetween sibIndex parent
      inbetweenRecord = (bRecords parent) !! inbetweenIndex
      promoteRecord = last $ bRecords sibPage
      sibToShift = last $ bChildren sibPage
      newPage = newBPage (inbetweenRecord : (bRecords page)) (sibToShift : (bChildren page)) 
      (leftPar, rightPar) = splitAt inbetweenIndex (bRecords parent)
      newRightPar = case null rightPar of
                      True -> []
                      False -> tail rightPar
      newParent = newBPage (leftPar ++ [promoteRecord] ++ newRightPar) (bChildren parent) 
      newSib = newBPage (L.delete (last $ bRecords sibPage) (bRecords sibPage))
               (L.delete (last (bChildren sibPage)) (bChildren sibPage))
borrowForIndex (FullPage index page) (FullPage pIndex parent) 
                   (Sibling sibIndex sibPage RightSibling) = 
    (FullPage index newPage, FullPage pIndex newParent, FullPage sibIndex newSib)
    where
      Just inbetweenIndex = findInbetween index parent
      inbetweenRecord = (bRecords parent) !! inbetweenIndex
      promoteRecord = head $ bRecords sibPage
      sibToShift = head $ bChildren sibPage
      newPage = newBPage ((bRecords page) ++ [inbetweenRecord]) 
                ((bChildren page) ++ [sibToShift])
      (leftPar, rightPar) = splitAt inbetweenIndex (bRecords parent)
      newRightPar = case null rightPar of
                      True -> []
                      False -> tail rightPar
      newParent = newBPage (leftPar ++ [promoteRecord] ++ newRightPar) (bChildren parent) 
      newSib = newBPage (tail $ bRecords sibPage) (tail $ bChildren sibPage)

mergeForIndex :: FullPage -> FullPage -> FullPage -> (FullPage, FullPage, FullPage)
mergeForIndex (FullPage index page) (FullPage pIndex parent) 
                  (Sibling sibIndex sibPage RightSibling) = 
    (FullPage index newPage, FullPage pIndex newParent, DeletePage sibIndex sibPage)
    where
      Just inbetweenIndex = findInbetween index parent
      inbetweenRecord = (bRecords parent) !! inbetweenIndex
      newParent = removeRecord inbetweenRecord parent
      newPage = newBPage ((bRecords page) ++ [inbetweenRecord] ++ (bRecords sibPage)) 
                ((bChildren page) ++ (bChildren sibPage))
mergeForIndex (FullPage index page) (FullPage pIndex parent) 
                  (Sibling sibIndex sibPage LeftSibling) = 
    (FullPage sibIndex newPage, FullPage pIndex newParent, DeletePage index sibPage)
    where
      Just inbetweenIndex = findInbetween sibIndex parent
      inbetweenRecord = (bRecords parent) !! inbetweenIndex
      newParent = removeRecord inbetweenRecord parent
      newPage = newBPage ((bRecords sibPage) ++ [inbetweenRecord] ++ (bRecords page)) 
                ((bChildren sibPage) ++ (bChildren page))

findSiblingIndex :: Sibling -> BIndex -> BPage -> Maybe BIndex
findSiblingIndex sibling index page = siblingIndex lIndex
    where 
      curIndex = L.findIndex (== index) (bChildren page)
      lIndex = case curIndex of
                 Just idx -> case idx == 0 of
                               True -> case sibling of
                                            LeftSibling -> Nothing
                                            RightSibling -> Just (idx + 1)
                               False -> case sibling of
                                             LeftSibling -> Just (idx - 1)
                                             RightSibling -> 
                                                 case idx == ((length (bChildren page)) - 1) of
                                                   True -> Nothing
                                                   False -> Just (idx + 1)
                 Nothing -> Nothing
      siblingIndex (Just idx) = Just ((bChildren page) !! idx)
      siblingIndex Nothing = Nothing
      

treeSearch :: (BTreeData b) => b -> BIndex -> BRecord -> IO (Maybe BResult)
treeSearch context rootIndex record
   | (isBSearch record) == True = 
      treeSearch context rootIndex (newBRecord (recordKey record) BNullData Nothing)
   | otherwise = do
      (index, _, page) <- findPage context record rootIndex []
      recResult <- return $ findRecord record page
      return $ maybe Nothing (\r -> Just $ BResult index r) recResult

data BResult = BResult { bindexResult :: BIndex
                       , brecordResult :: BRecord }
                     deriving Show

treeUpdate :: (BTreeData b) => b -> BIndex -> BRecord -> IO BIndex
treeUpdate context rootIndex record = do
   (index, parents, page) <- findPage context record rootIndex []
   let updatedPage = updatePage page record
   let pages = ((index, updatedPage):parents)
   updateTree context rootIndex pages []

updatePage :: BPage -> BRecord -> BPage
updatePage page record = newBPage newRecords (bChildren page)
   where
      records = (bRecords page)
      newRecords = foldl (\newRecords' oldRecord -> 
                              if oldRecord == record
                              then (newRecords' ++ [record])
                              else (newRecords' ++ [oldRecord])) 
                         [] records

treeRange :: (BTreeData b) => b -> BDataType -> BDataType -> IO [BRecord]
treeRange context minKey maxKey = do
  root <- getMasterRoot context
  Just rootPage <- getBPage context root
  sortedRange context rootPage minKey maxKey

sortedRange :: (BTreeData b) => b -> BPage -> BDataType -> BDataType -> IO [BRecord]
sortedRange context bpage minKey maxKey 
   | (L.length $ bChildren bpage) == 0 = do
      let records = bRecords bpage
          rmin = newBRecord minKey BNullData Nothing
          rmax = newBRecord maxKey BNullData Nothing
          newRecs = foldl (\arr rec -> if (and [rec >= rmin, rec <= rmax]) 
                                          then arr++[rec]
                                          else arr) [] records
      return newRecs
   | otherwise = do
      let minIndex = findIndex (newBRecord minKey BNullData Nothing) (bRecords bpage)
      let maxIndex = findIndex (newBRecord maxKey BNullData Nothing) (bRecords bpage)
      let (_, childs) = foldl (\ (i,arr) x -> case (and [i >= minIndex, i <= maxIndex]) of
                                                   True -> (i+1, (arr++[x]))
                                                   False -> (i+1, arr)) (0,[]) (bChildren bpage)
      recs <- mapM (\child -> do Just cpage <- getBPage context child
                                 let count = L.length $ bChildren cpage
                                     firstRec = L.head $ bRecords cpage
                                 case (and [firstRec > (newBRecord maxKey BNullData Nothing),
                                            count == 0]) of
                                      True -> return []
                                      False -> sortedRange context cpage minKey maxKey) childs
      let newRecs = concat recs
      return newRecs
